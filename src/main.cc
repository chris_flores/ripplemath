#include <iostream>
#include <cstdlib>
#include <string>
using namespace std;

#include "../inc/rippleMath.h"

int main(){

  //Get the integers from the user
  cout <<endl;
  cout <<"Enter an integer and then press enter (up to 10 digits in length)." <<endl;
  std::string a = Read();
  cout <<"Enter another and then press enter." <<endl;
  std::string b = Read();
  cout <<endl;

  //Perform the operations
  std::string sum = AddString(a,b);
  std::string product = MultiplyString(a,b);

  //Remove the Leading Zeros
  RemovePadding(sum);
  RemovePadding(product);

  //Report the results
  cout <<"----------------------------------" <<endl;
  cout <<"Sum: " <<sum <<endl;
  cout <<"Product: " <<product <<endl;
  cout <<"----------------------------------" <<endl;

  return 0;
}
