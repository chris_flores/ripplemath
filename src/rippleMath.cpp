#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>

#include "../inc/rippleMath.h"

using namespace std;

//_________________________________________________________
std::string Read(){

  //Read the contents of standard input and
  //return a string of length 10 that has been
  //front padded with zeros.
  
  std::string input;
  cin >> input;

  while (input.length() < 10){
    input.insert(0,1,'0');
  }
  
  return input;
  
}

//_________________________________________________________
void RemovePadding(std::string &s){

  //Remove the leading zeros from the string

  while(s.front() == '0'){
    s.erase(s.begin());
  }
  
}

//_________________________________________________________
std::string AddString(std::string a, std::string b){

  std::string sum(10,'0');

  //Create two quantities to hold the internal state
  std::pair<char,char> t0('0','0'); //(carry, val)
  std::pair<char,char> t1('0','0'); //(carry, val)

  //Starting from the back of the strings loop over their
  //digits and perform the sum.
  //Note: By construction the strings are the same length.
  for (int iDigit=a.size()-1; iDigit>=0; iDigit--){

    //Add the two digits 
    t0 = AddCharWithCarry(a.at(iDigit),b.at(iDigit));

    //Add the result to the value of t1 and store in sum
    sum.at(iDigit) = AddCharWithCarry(t0.second,t1.second).second;
    
    //Move the Carry bit to the value of the t1
    t1.first = '0';
    t1.second = t0.first;

  }
  
  return sum;
}

//________________________________________________________
std::string MultiplyString(std::string a, std::string b){

  std::string product(10,'0');

  //Create a vector of strings, one for each power of ten
  //of the product which will then get summed.
  std::vector<std::string> subTotals(10,product);

  //Loop Over All the digits starting from the
  //back of the strings
  int place(0);
  for (int i=a.size()-1; i>=0; i--){

    //Create two quantities to hold the internal state 
    std::pair<char,char> t0('0','0'); //(carry, val)
    std::pair<char,char> t1('0','0'); //(carry, val)
    
    for (int j=b.size()-1; j>=0; j--){

      t0 = MultiplyWithCarry(a.at(j),b.at(i));
      t1 = AddCharWithCarry(t0.second,t1.second);

      if (j-place > 0)
	subTotals.at(i).at(j-place) = t1.second;
      
      //Move the Carry Bit
      t1.second = t0.first;
      t1.first = '0';
      
    }//End Loop Over j
    place++;
  }//End Loop Over i

  //Sum All of the subTotal Strings
  for (unsigned int i=0; i<subTotals.size(); i++){
    product = AddString(product,subTotals.at(i));
  }
  
  return product;
  
}

//________________________________________________________
std::pair<char,char> AddCharWithCarry(char a, char b){

  //Wrapper for the AddChar Function which lets us
  //know if we have a carry value

  std::string result = AddChar(a,b);

  //If the result is only one digit long then there is no carry
  if (result.size() == 1)
    return std::make_pair('0',result.back());

  //Otherwise we have a carry
  return std::make_pair('1',result.back());

}

//________________________________________________________
std::pair<char,char> MultiplyWithCarry(char a, char b){

  //Wrapper for the MultiplyChar Function which lets
  //us know if we have a cary value

  std::string result = MultiplyChar(a,b);

  if (result.size() == 1)
    return std::make_pair('0',result.back());

  //Otherwise we have a carry
  return std::make_pair(result.at(result.size()-2),result.back());

}

//**********************************************************
//THE FUNCTIONS BELOW ARE THE ONES "GIVEN"  
//________________________________________________________
std::string AddChar(char a, char b){

  //Return the sum of two single characters
  //in the form of a string
  
  long int aN = std::atol(&a);
  long int bN = std::atol(&b);

  long int sum = aN+bN;

  return std::to_string(sum);

}

//________________________________________________________
std::string MultiplyChar(char a, char b){

  //Return the product of two single characters
  //in the form of a string
  
  long int aN = std::atol(&a);
  long int bN = std::atol(&b);

  long int product = aN*bN;

  return std::to_string(product);
  
}
