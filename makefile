main : src/main.cc libs
	g++ -std=c++11 -o main src/main.cc bin/rippleMath.o

libs : src/rippleMath.cpp inc/rippleMath.h
	g++ -std=c++11 -c -o bin/rippleMath.o src/rippleMath.cpp

clean:
	rm bin/rippleMath.o
	rm main
