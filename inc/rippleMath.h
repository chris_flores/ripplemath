std::string Read();
void RemovePadding(std::string &s);
std::string AddChar(char a, char b);
std::string MultiplyChar(char a, char b);
std::pair<char,char> AddCharWithCarry(char a, char b);
std::pair<char,char> MultiplyWithCarry(char a, char b);
std::string AddString(std::string a, std::string b);
std::string MultiplyString(std::string a, std::string b);
